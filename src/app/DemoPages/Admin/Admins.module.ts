import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminsRoutingModule} from "./Admins-routing.module";
import { StorageComponent } from './storage/storage.component';

@NgModule({
  imports: [
    CommonModule,
    AdminsRoutingModule
  ],
  declarations: [StorageComponent]
})
export class AdminsModule { }
