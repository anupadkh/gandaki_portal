import { Injectable, Inject } from '@angular/core';
import {LOCAL_STORAGE, WebStorageService, SESSION_STORAGE}  from 'angular-webstorage-service';
import { FormService } from 'src/app/services/form.service';
import { GridApi } from 'ag-grid-community';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() { }
}

export interface StoredRow{
   data : any;
   rowIndex: number;
}

export interface StorageDoc{
   rows: StoredRow[];
  
}

export function UpdateStored(currentData: any, storage: WebStorageService, tableid: number){
  let tableKey = 'table' + String(tableid);
  let stored_value = storage.get(tableKey);
  if (stored_value && Array.isArray(stored_value)){
    if (stored_value.find(x => x.rowIndex === currentData.rowIndex)){
      let myindex = stored_value.indexOf( stored_value.find(x => x.rowIndex === currentData.rowIndex ));
      stored_value[myindex] = currentData;
      storage.set(tableKey, stored_value);
    } else{
      stored_value.push(currentData);
      storage.set(tableKey, stored_value);
    }
  }else{
    storage.set(tableKey, [currentData]);
  }
 
}

export function RemoveStoredIndex(index: number, storage:WebStorageService, tableid:number){
  
  let tableKey = 'table' + String(tableid);
  let stored_value:StoredRow[] = storage.get(tableKey);
  
  if (stored_value.find(x => x.rowIndex == index)){
    let myindex = stored_value.indexOf( stored_value.find(x => x.rowIndex == index ));
    stored_value = stored_value.slice(0, myindex).concat(stored_value.slice(myindex+1));
    storage.set(tableKey,stored_value);
  }

}

class DataResponse{
  data:any[];
  extra_params:number[];
}

export function saveStored( 
    api:GridApi, 
    storage:WebStorageService, 
    tableid:number, 
    formService: FormService
  ){
  return new Promise((resolve, reject) => {
    let tableKey = 'table' + String(tableid);
    let submitData: StoredRow[] = storage.get(tableKey);
    let data = []; 
    let extra = [];
    submitData.forEach((myObj:StoredRow, index)=> {
      data.push(myObj.data);
      extra.push(myObj.rowIndex);
    });
    // console.log(data, extra);
    if(submitData.length !== 0){
      formService.saveData(tableid, data, extra).subscribe(
        (response:DataResponse) => {
          // debugger
          for (let index = 0; index < response.data.length; index++) {
            const data = response.data[index];
            const Rowindex = response.extra_params[index];
            
            api.getRowNode(Rowindex.toString()).data._id = response.data[index]._id;
            RemoveStoredIndex(Rowindex, storage, tableid);
          }
          if (storage.get(tableKey).length === 0){
            
          }
          resolve();
        }, err =>{
          reject();
        }
      )
    }
    resolve();
  })
  
}