"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.TableDetailsComponent = void 0;
var core_1 = require("@angular/core");
var sweetalert2_1 = require("sweetalert2");
var environment_1 = require("../../../../../environments/environment");
// import {TemporaryStorageFacet, TemporaryStorageService} from "../../storage/storage.service";
// import {StoredRow, StorageDoc} from './helper.service';
var angular_webstorage_service_1 = require("angular-webstorage-service");
var helper_service_1 = require("./helper.service");
var TableDetailsComponent = /** @class */ (function () {
    // private temporaryStorage: TemporaryStorageFacet;
    // public stored_data: StoredRow;
    function TableDetailsComponent(
    // private temporaryStorageService: TemporaryStorageService,
    storage, activatedRoute, formBuilder, formService, router, generalService, toastr) {
        this.storage = storage;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.formService = formService;
        this.router = router;
        this.generalService = generalService;
        this.toastr = toastr;
        this.loading = false;
        this.submitted = false;
        this.error = '';
        this.columnDefs = [];
        this.colData = [];
        this.rowData = [];
        this.fields = [];
        this.title = 'app';
        this.buttonText = 'Saved';
        this.btnClass = 'btn-success';
        this.adminUrl = environment_1.environment.adminUrl;
        this.apiUrl = environment_1.environment.apiUrl;
        this.scroll = function (event) {
            event.preventDefault();
        };
        this.autoGroupColumnDef = { minWidth: 200 };
        this.defaultColDef = {
            enableRowGroup: true,
            enablePivot: true,
            enableValue: true,
            sortable: true,
            resizable: true,
            filter: true,
            filterParams: { newRowsAction: 'keep' }
            // valueFormatter: 'currencyDollarFormatter'
        };
        this.sideBar = {
            toolPanels: [
                'filters',
                {
                    id: 'columns',
                    labelDefault: 'Columns',
                    labelKey: 'columns',
                    iconKey: 'columns',
                    toolPanel: 'agColumnsToolPanel',
                    toolPanelParams: { suppressSyncLayoutWithGrid: true }
                },
            ]
        };
        this.statusBar = {
            statusPanels: [
                {
                    statusPanel: 'agTotalRowCountComponent',
                    align: 'left',
                    key: 'totalRowComponent'
                },
                {
                    statusPanel: 'agFilteredRowCountComponent',
                    align: 'left'
                },
                {
                    statusPanel: 'agSelectedRowCountComponent',
                    align: 'center'
                },
                {
                    statusPanel: 'agAggregationComponent',
                    align: 'right'
                },
            ]
        };
        this.ColumnAddForm = this.formBuilder.group({
            newcolumn: ''
        });
        this.editType = 'fullRow';
        this.colIds = [];
        this.indexIds = [];
        this.tableState = {};
        this.rowErrors = [];
        this.rowSuccess = [];
    }
    TableDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (paramsId) {
            _this.id = paramsId.id;
            _this.gridId = '#agGridTable' + _this.id;
            _this.generalService.getTableDetails(_this.id).subscribe(function (response) {
                _this.pageTitle = response.data.nepali_name;
            });
            _this.renderTable(true);
            _this.restoreState();
            // this.temporaryStorage = this.temporaryStorageService.forKey( this.id );
        });
        window.addEventListener('scroll', this.scroll, true);
    };
    TableDetailsComponent.prototype.ngOnDestroy = function () {
        window.removeEventListener('scroll', this.scroll, true);
    };
    TableDetailsComponent.prototype.renderTable = function (isOnInit) {
        var _this = this;
        this.formService.getDataHeaders(this.id).subscribe(function (response) {
            _this.row_headers = response.rows_headers;
            _this.col_headers = response.col_headers;
            _this.index_cols = response.index_cols;
            _this.empty_table = response.empty_table;
            _this.rowData = [];
            _this.gridApi.setRowData([]);
            if (isOnInit == true) {
                _this.colData = [];
                _this.gridApi.setColumnDefs(_this.colData);
                _this.colData.push({
                    headerName: '_id', value: '_id', hide: true, suppressToolPanel: true, 'field': '_id'
                });
                _this.colIds.push('_id');
                /*this.colData.push(
                  {headerName: '', field: 'group', pinned: 'left',  cellStyle: {color: 'white', 'background-color': '#1c77b9'}}
                )
                this.colIds.push('group');*/
                for (var i = 0; i < _this.row_headers.indicators[0].group.length; i++) {
                    _this.colData.push({ headerName: '', field: 'group' + i, pinned: 'left', cellStyle: { color: 'white', 'background-color': '#1c77b9' } });
                    _this.colIds.push('group' + i);
                }
                _this.colData.push({ headerName: _this.row_headers.title, field: 'row', pinned: 'left', cellStyle: { color: 'white', 'background-color': '#1c77b9' } });
                _this.colIds.push(_this.row_headers.title);
                _this.indexIds.push('row');
                _this.index_cols.forEach(function (value) {
                    if (value.options.length > 0) {
                        var currentOptions_1 = [];
                        value.options.forEach(function (option) {
                            currentOptions_1.push(option.nepali_name);
                        });
                        _this.colData.push({
                            headerName: value.title,
                            field: value.col_id,
                            sortable: true,
                            filter: true,
                            cellEditor: 'agSelectCellEditor',
                            cellEditorParams: {
                                values: currentOptions_1
                            },
                            type: 'Select',
                            options: value.options
                        });
                    }
                    else {
                        _this.colData.push({
                            headerName: value.title,
                            field: value.col_id,
                            sortable: true,
                            filter: true
                        });
                    }
                    _this.colIds.push(value.col_id);
                    _this.indexIds.push(value.col_id);
                });
                _this.col_headers.forEach(function (items) {
                    _this.modifyColumnHeaders(items);
                    _this.colData.push(items);
                });
                _this.gridApi.setColumnDefs(_this.colData);
                _this.gridApi.setHeaderHeight(30);
                _this.restoreState();
                // this.restoreState();
            }
            _this.empty_table.forEach(function (row) {
                var usedKeys = [];
                var rowValue = {};
                var currentRowValue = '';
                var currentGroupValue = '';
                var currentRowID = '';
                _this.row_headers.indicators.forEach(function (indicator) {
                    if (indicator.id == row.row) {
                        currentRowValue = indicator.title;
                        currentRowID = indicator.id;
                        for (var i = 0; i < indicator.group.length; i++) {
                            rowValue['group' + i] = indicator.group[i];
                            usedKeys.push('group' + i);
                        }
                        // currentGroupValue = indicator.group[0];
                    }
                });
                // rowValue['group'] = currentGroupValue;
                rowValue['row'] = currentRowValue;
                rowValue['row_id'] = currentRowID;
                // usedKeys.push('group');
                usedKeys.push('row');
                usedKeys.push('row_id');
                var keys = Object.keys(row);
                keys.forEach(function (key) {
                    if (key == 'row') {
                        return;
                    }
                    if (key == 'row_id') {
                        return;
                    }
                    if (key == 'group') {
                        return;
                    }
                    if (usedKeys.indexOf(key) > -1) {
                        return;
                    }
                    var indexValue = '';
                    _this.index_cols.forEach(function (index_col) {
                        if (index_col.col_id == key) {
                            index_col.options.forEach(function (option) {
                                if (option.id == row[key]) {
                                    indexValue = option.nepali_name;
                                    usedKeys.push(key);
                                }
                            });
                        }
                    });
                    rowValue[key] = indexValue;
                });
                keys.forEach(function (key) {
                    var keyStatus = false;
                    usedKeys.forEach(function (usedKey) {
                        if (usedKey == key) {
                            keyStatus = true;
                            return;
                        }
                    });
                    // tslint:disable-next-line:triple-equals
                    // @ts-ignore
                    if (keyStatus == true) {
                        return;
                    }
                    rowValue[key] = row[key];
                });
                // console.log("Row value after empty tables");
                // console.log(rowValue);
                _this.rowData.push(rowValue);
            });
            _this.formService.getData(_this.id).subscribe(function (dataResponse) {
                _this.apiData = dataResponse.data;
                _this.rowData.forEach(function (row) {
                    var currentRowValue = '';
                    /*this.row_headers.indicators.forEach(indicator => {
                      if (indicator.title == row.row) {
                        currentRowValue = indicator.id;
                      }
                    });*/
                    row['row'] = row['row_id'];
                    var usedKeys = [];
                    usedKeys.push('row');
                    usedKeys.push('row_id');
                    var keys = Object.keys(row);
                    keys.forEach(function (key) {
                        if (key == 'row') {
                            return;
                        }
                        if (key == 'row_id') {
                            return;
                        }
                        var indexValue = '';
                        _this.index_cols.forEach(function (index_col) {
                            if (index_col.col_id == key) {
                                index_col.options.forEach(function (option) {
                                    if (option.nepali_name == row[key]) {
                                        indexValue = option.id;
                                        usedKeys.push(key);
                                    }
                                });
                            }
                        });
                        row[key] = indexValue;
                    });
                    _this.apiData.forEach(function (apiDatum) {
                        var matchStatus = true;
                        _this.indexIds.forEach(function (indexId) {
                            if (matchStatus == false) {
                                return;
                            }
                            if (row[indexId] == apiDatum[indexId]) {
                                matchStatus = true;
                            }
                            else {
                                matchStatus = false;
                            }
                        });
                        if (matchStatus == true) {
                            var apiDataKeys = Object.keys(apiDatum);
                            apiDataKeys.forEach(function (apiDataKey) {
                                var isIndexKey = false;
                                _this.indexIds.forEach(function (indexId) {
                                    if (indexId == apiDataKey) {
                                        isIndexKey = true;
                                        return;
                                    }
                                });
                                // tslint:disable-next-line:triple-equals
                                // @ts-ignore
                                if (isIndexKey == true) {
                                    return;
                                }
                                row[apiDataKey] = apiDatum[apiDataKey];
                            });
                        }
                    });
                    //  Modify data to get the headers
                    var rowValue = {};
                    currentRowValue = '';
                    var currentGroupValue = '';
                    usedKeys = [];
                    _this.row_headers.indicators.forEach(function (indicator) {
                        if (indicator.id == row.row) {
                            currentRowValue = indicator.title;
                            for (var i = 0; i < indicator.group.length; i++) {
                                row['group' + i] = indicator.group[i];
                                usedKeys.push('group' + i);
                            }
                            // currentGroupValue = indicator.group[0];
                        }
                    });
                    // row['group'] = currentGroupValue;
                    row['row'] = currentRowValue;
                    //
                    usedKeys.push('_id');
                    // usedKeys.push('group')
                    usedKeys.push('row');
                    //
                    _this.indexIds.forEach(function (key) {
                        if (usedKeys.indexOf(key) !== -1) {
                            return;
                        }
                        /*if (key == 'row') {
                          return;
                        }
            
                        if(key == 'group'){
                          return;
                        }
            
                        if(usedKeys.indexOf(key) > -1){
                          return;
                        }*/
                        var indexValue = '';
                        _this.index_cols.forEach(function (index_col) {
                            if (index_col.col_id == key) {
                                index_col.options.forEach(function (option) {
                                    if (option.id == row[key]) {
                                        indexValue = option.nepali_name;
                                        usedKeys.push(key);
                                    }
                                });
                            }
                        });
                        row[key] = indexValue;
                    });
                    _this.gridColumnApi.getAllColumns().forEach(function (column) {
                        var userColDef = column.getUserProvidedColDef();
                        if (_this.indexIds.indexOf(userColDef.field) !== -1) {
                        }
                        else {
                            if (userColDef.field == '_id' || userColDef.field == 'group') {
                                return;
                            }
                            if (userColDef.type == 'Select') {
                                userColDef.options.forEach(function (option) {
                                    if (option.id == row[userColDef.field]) {
                                        row[userColDef.field] = option.nepali_name;
                                    }
                                });
                            }
                            else if (userColDef.type == 'Rupees') {
                                row[userColDef.field] = _this.curencyNepaliRupeesFormatter(row[userColDef.field]);
                            }
                        }
                    });
                });
                _this.gridApi.setRowData([]);
                _this.gridApi.setRowData(_this.rowData);
                // console.log("Row data issye");
                // console.log(this.rowData);
                var allColumnIds = [];
                _this.gridColumnApi.getAllColumns().forEach(function (column) {
                    allColumnIds.push(column.colId);
                });
            });
        });
    };
    TableDetailsComponent.prototype.addNewRow = function () {
        this.rowData.push({});
        this.gridApi.setRowData(this.rowData);
    };
    TableDetailsComponent.prototype.onGridReady = function (params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        // this.gridApi.setColumnDefs([]);
        this.renderTable(false);
    };
    TableDetailsComponent.prototype.onBtStopEditing = function () {
        this.gridApi.stopEditing();
    };
    TableDetailsComponent.prototype.navigateToDesignTables = function () {
        this.router.navigate(['admin/tables/design/' + this.id]);
    };
    TableDetailsComponent.prototype.navigateToCharts = function () {
        this.router.navigate(['admin/tables/charts/' + this.id]);
    };
    TableDetailsComponent.prototype.navigateToPreview = function () {
        this.router.navigate(['guest/tables/details/' + this.id]);
    };
    TableDetailsComponent.prototype.onCellClicked = function ($event) {
        // check whether the current row is already opened in edit or not
        if (this.editingRowIndex != $event.rowIndex) {
            // console.log($event);
            $event.api.startEditingCell({
                rowIndex: $event.rowIndex,
                colKey: $event.column.colId
            });
            this.editingRowIndex = $event.rowIndex;
        }
        this.buttonText = 'Save Data';
        this.btnClass = '';
    };
    TableDetailsComponent.prototype.onCellEditingStopped = function ($event) {
        var editedRowIndex = $event.rowIndex;
        this.updatedData = $event.data;
    };
    TableDetailsComponent.prototype.onSaveButtonClicked = function () {
        this.gridApi.stopEditing();
    };
    TableDetailsComponent.prototype.onRowEditingStopped = function ($event, fromRow) {
        if (fromRow === void 0) { fromRow = true; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.saveRowData($event, true)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TableDetailsComponent.prototype.onPasteEnd = function (params) {
        var self = this;
        this.gridApi.forEachNode(function (node) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, self.saveRowData(node, false)];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        });
    };
    TableDetailsComponent.prototype.onColumnResized = function ($event) {
        var columnDetails = $event.columns[0];
    };
    TableDetailsComponent.prototype.modifyColumnHeaders = function (obj) {
        var _this = this;
        if (obj.hasOwnProperty('children')) {
            var columnChildrens = obj.children;
            columnChildrens.forEach(function (columnChildren) {
                if (columnChildren.type == 'Select') {
                    var currentOptions_2 = [];
                    columnChildren.options.forEach(function (option) {
                        currentOptions_2.push(option.nepali_name);
                    });
                    columnChildren.cellEditor = 'agSelectCellEditor';
                    columnChildren.cellEditorParams = {
                        values: currentOptions_2
                    };
                }
                else if (columnChildren.type == 'Number') {
                    columnChildren.cellStyle = { 'text-align': 'right', 'font-family': 'Fontasy Himali' };
                }
                else if (columnChildren.type == 'Rupees') {
                    columnChildren.cellStyle = { 'text-align': 'right', 'font-family': 'Fontasy Himali' };
                }
                else if (columnChildren.type == 'Float') {
                    columnChildren.cellStyle = { 'text-align': 'right', 'font-family': 'Fontasy Himali' };
                }
                else if (columnChildren.type == 'Percentage') {
                    columnChildren.cellStyle = { 'text-align': 'right', 'font-family': 'Fontasy Himali' };
                }
                _this.modifyColumnHeaders(columnChildren);
            });
        }
        return null;
    };
    TableDetailsComponent.prototype.curencyNepaliRupeesFormatter = function (params) {
        return (Math.round(params * 100) / 100).toFixed(2);
        // return this.formatNumber(params);
    };
    TableDetailsComponent.prototype.currencyDollarFormatter = function (params) {
        // console.log(params);
    };
    TableDetailsComponent.prototype.formatNumber = function (number) {
        var formatter = new Intl.NumberFormat('np-NP', {
            style: 'currency',
            currency: 'रु ',
            minimumFractionDigits: 2
        });
        return formatter.format(number);
    };
    // public async getUnSavedRowData(): Promise<void>{
    //   var cachedFormData = await this.temporaryStorage.get<StoredRow>();
    // 	if ( cachedFormData ) {
    // 		Object.assign( this.stored_data, cachedFormData );
    // 	}
    // }
    TableDetailsComponent.prototype.saveRowData = function ($event, showAlert) {
        var _this = this;
        if (showAlert === void 0) { showAlert = false; }
        var row = $event.data;
        // console.log("Data to be saved");
        // console.log(row);
        var saveColData = {};
        this.gridColumnApi.getAllColumns().forEach(function (column) {
            var userColumnDef = column.getUserProvidedColDef();
            if (userColumnDef.type == 'Select') {
                // console.log(userColumnDef);
                userColumnDef.options.forEach(function (option) {
                    if (option.nepali_name == row[userColumnDef.field]) {
                        saveColData[userColumnDef.field] = option.id;
                    }
                });
            }
            else {
                if (row[userColumnDef.field]) {
                    saveColData[userColumnDef.field] = row[userColumnDef.field];
                }
                else {
                    if (userColumnDef.field != '_id') {
                        saveColData[userColumnDef.field] = "";
                    }
                }
            }
        });
        var row_header_row = 'row';
        this.row_headers.indicators.forEach(function (indicator) {
            if (indicator.title == saveColData[row_header_row]) {
                saveColData[row_header_row] = indicator.id;
            }
        });
        var currentData = {
            'data': saveColData,
            'rowIndex': $event.rowIndex
        };
        var responseId = '';
        helper_service_1.UpdateStored(currentData, this.storage, this.id);
        helper_service_1.RemoveStoredIndex(currentData.rowIndex, this.storage, this.id);
        // var storage_data : StoredRow;
        // storage_data = { row_index: $event.rowIndex, row_object: saveColData}
        // // var storageDoc =
        // this.temporaryStorage.set( storage_data );
        return new Promise(function (resolve, reject) {
            _this.formService.saveData(_this.id, currentData).subscribe(function (response) {
                responseId = response.data[0]._id;
                _this.buttonText = 'Saved';
                _this.btnClass = 'btn-success';
                $event.data._id = responseId;
                if (showAlert) {
                    // this.showAlert("Data saved successfully");
                }
                _this.rowSuccess.push($event.node.rowIndex);
                _this.setRowStyle();
                resolve();
            }, function (err) {
                _this.rowErrors.push($event.node.rowIndex);
                _this.setRowStyle();
                reject();
                // $event.data;
            });
        });
    };
    TableDetailsComponent.prototype.showAlert = function (message) {
        this.swalSuccess(message);
    };
    TableDetailsComponent.prototype.saveTableConfiguration = function () {
        var _this = this;
        this.tableState.tableID = this.id;
        this.tableState.colState = this.gridColumnApi.getColumnState();
        this.tableState.groupState = this.gridColumnApi.getColumnGroupState();
        this.tableState.filterState = this.gridApi.getFilterModel();
        var currentData = {
            data: this.tableState
        };
        console.log(currentData);
        this.generalService.saveTableState(currentData).subscribe(function (response) {
            _this.tableState._id = response.data[0]._id;
            _this.showAlert("Table configuration saved successfully");
        });
    };
    TableDetailsComponent.prototype.restoreState = function () {
        var _this = this;
        this.generalService.getTableState(this.id).subscribe(function (response) {
            if (response.data[0]) {
                // console.log(response.data[0]);
                _this.tableState._id = response.data[0]._id;
                _this.tableState.tableID = response.data[0].tableID;
                _this.tableState.colState = response.data[0].colState;
                _this.tableState.groupState = response.data[0].groupState;
                _this.tableState.filterState = response.data[0].filterState;
                // console.log(this.tableState.filterState);
                _this.gridColumnApi.setColumnState(_this.tableState.colState);
                _this.gridColumnApi.setColumnGroupState(_this.tableState.groupState);
                _this.gridApi.setFilterModel(_this.tableState.filterState);
            }
            else {
                _this.tableState.tableID = _this.id;
                _this.tableState.colState = _this.gridColumnApi.getColumnState();
                _this.tableState.groupState = _this.gridColumnApi.getColumnGroupState();
                _this.tableState.filterState = _this.gridApi.getFilterModel();
            }
        });
    };
    TableDetailsComponent.prototype.swalSuccess = function (message) {
        sweetalert2_1["default"].fire({
            icon: 'success',
            title: message,
            showConfirmButton: false,
            timer: 1500
        });
    };
    TableDetailsComponent.prototype.setRowStyle = function () {
        // debugger
        // this.gridApi.gridOptionsWrapper.getRowStyle = (params) =>{
        //   if (params.node.rowIndex in this.rowErrors) {
        //     return { background: 'red' };
        //   } 
        //   if (params.node.rowIndex in this.rowSuccess){
        //     return {background:'green'};
        //   }
        // }
    };
    TableDetailsComponent = __decorate([
        core_1.Component({
            selector: 'app-table-details',
            templateUrl: './table-details.component.html',
            styleUrls: ['./table-details.component.css']
        }),
        __param(0, core_1.Inject(angular_webstorage_service_1.SESSION_STORAGE))
    ], TableDetailsComponent);
    return TableDetailsComponent;
}());
exports.TableDetailsComponent = TableDetailsComponent;
