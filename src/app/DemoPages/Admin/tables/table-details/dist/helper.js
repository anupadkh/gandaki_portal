var StoredRow = /** @class */ (function () {
    function StoredRow(idx, row_object) {
        this.row_index = idx;
        this.row_object = row_object;
    }
    return StoredRow;
}());
var StorageDoc = /** @class */ (function () {
    function StorageDoc(rows) {
        if (typeof (rows) === "object") {
            if (Array.isArray(rows)) {
                this.rows = rows;
            }
            else {
                this.rows = [rows];
            }
        }
        else {
            throw Error("You should pass an StoredRow Object or Array");
        }
    }
    return StorageDoc;
}());
