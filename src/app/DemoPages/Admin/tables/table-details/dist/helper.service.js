"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RemoveStoredIndex = exports.UpdateStored = exports.HelperService = void 0;
var core_1 = require("@angular/core");
var HelperService = /** @class */ (function () {
    function HelperService() {
    }
    HelperService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], HelperService);
    return HelperService;
}());
exports.HelperService = HelperService;
function UpdateStored(currentData, storage, tableid) {
    var tableKey = 'table' + String(tableid);
    var stored_value = storage.get(tableKey);
    if (stored_value && Array.isArray(stored_value)) {
        if (stored_value.find(function (x) { return x.rowIndex === currentData.rowIndex; })) {
            var myindex = stored_value.indexOf(stored_value.find(function (x) { return x.rowIndex === currentData.rowIndex; }));
            stored_value[myindex] = currentData;
            storage.set(tableKey, stored_value);
        }
        else {
            stored_value.push(currentData);
            storage.set(tableKey, stored_value);
        }
    }
    else {
        storage.set(tableKey, [currentData]);
    }
}
exports.UpdateStored = UpdateStored;
function RemoveStoredIndex(index, storage, tableid) {
    var tableKey = 'table' + String(tableid);
    var stored_value = storage.get(tableKey);
    if (stored_value.find(function (x) { return x.rowIndex === index; })) {
        // debugger
        var myindex = stored_value.indexOf(stored_value.find(function (x) { return x.rowIndex === index; }));
        stored_value = stored_value.slice(0, myindex).concat(stored_value.slice(myindex + 1));
        storage.set(tableKey, stored_value);
    }
}
exports.RemoveStoredIndex = RemoveStoredIndex;
