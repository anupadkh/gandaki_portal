import { ColHeader, IndexColHeader } from 'src/app/models/user';

export class Aggregates{
    axis: IndexColHeader;
    rule: string = "sum"; // sum, mean, count, mean, max, specific
    select_values: [] | string = "all" ;

}

export class ChartConfig{
    x_axis: IndexColHeader | ColHeader[];
    x_axis_groups: any;
    y_axis: ColHeader;
    aggregate_parameters: Aggregates[];
    header: string;
}

export class FilterParameters{
    axis: string;
    type: number; // 1 = number; 2= string; 3=percentage; 4=date;
    rule: number;// 1= "equals to"; 2 = less than, 3 = greater than, 4 = like
    select_values: [] | string = "all"; // none or list of values
}

class AxisConfig{
    id: number | string;
    type : string; // index, row, col_header + extra_col
    legend: string;
    options: any[];

    constructor(id, axis_type, legend ="", options=[]){
        this.id = id;
        this.type = axis_type;
        this.legend = legend;
        this.options = options;
    }
}

export class BarChartStoredConfig{
    x_axes: AxisConfig[];
    y_axis: AxisConfig;
    filter_parameters: FilterParameters[] = [];
    aggregate_rule = "sum"; // sum, mean, max, mean, count, min
    header: string;
    x_axes_legend ?: string[];
    y_axis_legend ?: string;
    plot_options = {
        bar:{
            horizontal: true
        }
    };
    chart_options = { 
        height: '350', 
        type:'bar'
    };
    colors? = [];
}



export function filterData(data:[], config:BarChartStoredConfig){
    if (config==undefined){
        config = new BarChartStoredConfig() ;
        config.y_axis = new AxisConfig('7_0_26', 'col_header', "परिमाण");
        config.x_axes =  [
            new AxisConfig("title" , 'row_header', "", ["१.१.१ गरिबी प्रतिदिन १.९ डलर (PPP मूल्य क्रयशक्तिको आधारमा)", 
            "१.१.२ प्रति व्यक्ति कुल राष्ट्रिय आम्दानी", 
            "१.२.१ राष्ट्रिय गरीबी रेखामुनि रहेका जनसङ्ख्याको प्रतिशत", 
            "१.२.२ राष्ट्रिय गरीबी रेखामुनि रहेका सबै उमेरका महिलाको प्रतिशत", 
            "१.२.३ बहुआयामिक गरिबी सूचकाङ्क (ब.ग.सू)", 
            "१.२.४ राष्ट्रिय गरीबी रेखामुनि रहेका ५ बर्षमूनिका बालबालिकाको प्रतिशत", 
            "१.३.१ PPP मुल्यमा प्रतिदिन १.९ अमेरिकी डलरभन्दा कम आम्दानी गर्ने रोजगारी प्राप्त व्यक्तिहरू", 
            "१.३.२ ३० मिनटको पैदलयात्रामा बजारसम्म पहुँच भएका घरपरिवारहरू", 
            "१.४.२ तल्लो पञ्चमक (Bottom quintile) को राष्ट्रिय उपभोगमा हिस्सा", 
            "१.४.३ विपद्का घटनाहरूबाट मृत्यु"])
        ]
        config.header = "Demo Test"
    }

    var mycase: boolean;  var mydata_filtered;
    var mydata = new Array();
    config.filter_parameters.forEach(function (myfilter){
        
        mydata_filtered = (data.filter( function (record) {
            mycase = true;
            if (typeof(myfilter.select_values) == "object"){
                myfilter.select_values.forEach( function (baseCase) {
                    mycase = (record[myfilter.axis] === baseCase) && mycase
                });
            }
            return mycase;
        }));
        mydata.concat(mydata_filtered)
    });
    if (mydata.length == 0){
        mydata = data;
    }
    var final_data = []
    config.x_axes.forEach( function (axis) {
        var axis_data = []
        axis.options.forEach(
            function (option){
                // var example = [{a:1, b:2, c:3}, {a:4, b:5, c:6}, {a:7, b:8, c:9}]
                try {
                    axis_data.push (mydata.filter(
                            y => ((option === y[axis.id]) && y[config.y_axis.id] !== undefined))
                        .map(function (record){
                            return record[config.y_axis.id]
                        }).reduce(reducer)
                    );    
                } catch (error) {
                    axis_data.push(undefined)
                }
                
            }
        )
        final_data.push(axis_data);
    })
    return final_data;
    
}

const reducer = (accumulator, currentValue) => accumulator + currentValue;