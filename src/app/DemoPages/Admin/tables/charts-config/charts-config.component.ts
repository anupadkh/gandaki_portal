import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TableChartsService } from 'src/app/DemoPages/Guest/charts/table-charts.service';
import { Table } from 'src/app/DemoPages/Guest/tables/dev-extreme/data/app.service';
import { Logger } from 'src/app/services/logger.service';
import { filterData } from './configInterface';


@Component({
  selector: 'app-charts-config',
  templateUrl: './charts-config.component.html',
  styleUrls: ['./charts-config.component.sass']
})
export class ChartsConfigComponent implements OnInit {
  
  table ; id; pageTitle; plotData;
  
  constructor(
    private chartService: TableChartsService,
    private activatedRoute: ActivatedRoute,
    private logger: Logger
  ) {
    this.table = new Table(); this.pageTitle = ""; this.plotData = []
   }

  ngOnInit() {
    this.activatedRoute.params.subscribe(paramsId => {
      
      this.id = paramsId.id;
      this.logger.log(this.id);

      this.chartService.get.event(paramsId).then( (table)=>{
        this.pageTitle = table.pageTitle;
        this.table = table;
        this.logger.log(table);
        
        this.plotData = filterData(this.table.data, undefined)
        this.logger.log( this.plotData );
        }
      );
    });
  }

}
