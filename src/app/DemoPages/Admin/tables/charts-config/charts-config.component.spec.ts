import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsConfigComponent } from './charts-config.component';

describe('ChartsConfigComponent', () => {
  let component: ChartsConfigComponent;
  let fixture: ComponentFixture<ChartsConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
