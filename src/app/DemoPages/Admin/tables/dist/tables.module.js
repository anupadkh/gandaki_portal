"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.TablesModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var angular_font_awesome_1 = require("angular-font-awesome");
var ng2_charts_1 = require("ng2-charts");
var ng_apexcharts_1 = require("ng-apexcharts");
var ngx_perfect_scrollbar_1 = require("ngx-perfect-scrollbar");
var ngx_perfect_scrollbar_2 = require("ngx-perfect-scrollbar");
var page_title_module_1 = require("../../../Layout/Components/page-title/page-title.module");
var tables_routing_module_1 = require("./tables-routing.module");
var tables_component_1 = require("./tables.component");
var ag_grid_angular_1 = require("ag-grid-angular");
require("ag-grid-enterprise");
var table_data_entry_component_1 = require("./table-data-entry/table-data-entry.component");
var table_designer_component_1 = require("./table-designer/table-designer.component");
var table_details_component_1 = require("./table-details/table-details.component");
var card_1 = require("@angular/material/card");
var tabs_1 = require("@angular/material/tabs");
var icon_1 = require("@angular/material/icon");
var checkBox_renderer_component_1 = require("../checkBox-renderer.component");
var table_indicators_component_1 = require("./table-indicators/table-indicators.component");
var table_charts_component_1 = require("./table-charts/table-charts.component");
var formTables_component_1 = require("./table-forms/formTables.component");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var form_field_1 = require("@angular/material/form-field");
var select_1 = require("@angular/material/select");
var forms_1 = require("@angular/forms");
var angular_webstorage_service_1 = require("angular-webstorage-service");
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};
var TablesModule = /** @class */ (function () {
    function TablesModule() {
    }
    TablesModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule, angular_font_awesome_1.AngularFontAwesomeModule, ng2_charts_1.ChartsModule, ngx_perfect_scrollbar_1.PerfectScrollbarModule,
                tables_routing_module_1.TablesRoutingModule, page_title_module_1.PageTitleModule, ng_apexcharts_1.NgApexchartsModule, ag_grid_angular_1.AgGridModule.withComponents([checkBox_renderer_component_1.CheckboxRenderer]),
                card_1.MatCardModule,
                tabs_1.MatTabsModule,
                icon_1.MatIconModule,
                ng2_charts_1.ChartsModule,
                ng_bootstrap_1.NgbModule,
                form_field_1.MatFormFieldModule, select_1.MatSelectModule, forms_1.FormsModule,
                angular_webstorage_service_1.StorageServiceModule
            ],
            declarations: [tables_component_1.TablesComponent, table_data_entry_component_1.TableDataEntryComponent, table_designer_component_1.TableDesignerComponent, table_details_component_1.TableDetailsComponent, checkBox_renderer_component_1.CheckboxRenderer, table_indicators_component_1.TableIndicatorsComponent, table_charts_component_1.TableChartsComponent, formTables_component_1.FormTablesComponent],
            providers: [
                {
                    provide: ngx_perfect_scrollbar_2.PERFECT_SCROLLBAR_CONFIG,
                    // DROPZONE_CONFIG,
                    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
                }
            ]
        })
    ], TablesModule);
    return TablesModule;
}());
exports.TablesModule = TablesModule;
