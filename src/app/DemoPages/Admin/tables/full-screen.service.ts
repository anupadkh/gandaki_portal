import {Injectable} from '@angular/core';
import { ThemeOptions } from 'src/app/theme-options';

@Injectable({
  providedIn: 'root',
})
export class FullscreenService {
  constructor(public theme_sidebar: ThemeOptions){

  }
  
  private doc = <FullScreenDocument>document;
  

  enter() {
    const el = this.doc.documentElement;
    if (el.requestFullscreen) el.requestFullscreen();
    else if (el.msRequestFullscreen) el.msRequestFullscreen();
    else if (el.mozRequestFullScreen) el.mozRequestFullScreen();
    else if (el.webkitRequestFullscreen) el.webkitRequestFullscreen();
  }

  leave() {
    if (this.doc.exitFullscreen) this.doc.exitFullscreen();
    else if (this.doc.msExitFullscreen) this.doc.msExitFullscreen();
    else if (this.doc.mozCancelFullScreen) this.doc.mozCancelFullScreen();
    else if (this.doc.webkitExitFullscreen) this.doc.webkitExitFullscreen();
  }

  toggle() {
    if (this.enabled) {
      this.leave();
      this.theme_sidebar.fullScreenMode = false;
      this.theme_sidebar.toggleSidebar = false;
    }
    else {
      this.enter();
      this.theme_sidebar.fullScreenMode = true;
      this.theme_sidebar.toggleSidebar = true;
    };
    this.theme_sidebar.sidebarHover = !this.theme_sidebar.toggleSidebar;
  }

  get enabled() {
    return !!(
      this.doc.fullscreenElement ||
      this.doc.mozFullScreenElement ||
      this.doc.webkitFullscreenElement ||
      this.doc.msFullscreenElement
    );
  }
}

interface FullScreenDocument extends HTMLDocument {
  documentElement: FullScreenDocumentElement;
  mozFullScreenElement?: Element;
  msFullscreenElement?: Element;
  webkitFullscreenElement?: Element;
  msExitFullscreen?: () => void;
  mozCancelFullScreen?: () => void;
  webkitExitFullscreen?: () => void;
}

interface FullScreenDocumentElement extends HTMLElement {
  msRequestFullscreen?: () => void;
  mozRequestFullScreen?: () => void;
  webkitRequestFullscreen?: () => void;
}

