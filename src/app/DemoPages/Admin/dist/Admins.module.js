"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.AdminsModule = void 0;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var Admins_routing_module_1 = require("./Admins-routing.module");
var storage_component_1 = require("./storage/storage.component");
var AdminsModule = /** @class */ (function () {
    function AdminsModule() {
    }
    AdminsModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                Admins_routing_module_1.AdminsRoutingModule
            ],
            declarations: [storage_component_1.StorageComponent]
        })
    ], AdminsModule);
    return AdminsModule;
}());
exports.AdminsModule = AdminsModule;
