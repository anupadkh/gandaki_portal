import { TestBed } from '@angular/core/testing';

import { TableChartsService } from './table-charts.service';

describe('TableChartsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableChartsService = TestBed.get(TableChartsService);
    expect(service).toBeTruthy();
  });
});
