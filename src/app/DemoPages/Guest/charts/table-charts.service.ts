import {FormBuilder} from "@angular/forms";
import {FormService} from "../../../services/form.service";
import {GeneralService} from "../../../services/general.service";
import {
  DataHeaderResponse,
  TableDetailsResponse,
  ChartsResponse,
  ColHeader,
  IndexColHeader
} from "../../../models/user";
import {AuthenticationService} from "../../../services/authentication.service";
import { Injectable } from '@angular/core';

import {TableHeader, Table} from '../tables/dev-extreme/data/app.service'
import { ActivatedRoute, Router } from '@angular/router';
import { Logger } from 'src/app/services/logger.service';
import { BarChartStoredConfig } from '../../Admin/tables/charts-config/configInterface';
@Injectable({
  providedIn: 'root'
})
export class TableChartsService {
  grouplength = 0; tableParameters: Table;


  constructor(private activatedRoute: ActivatedRoute,
    private formService: FormService,
    private router: Router,
    private generalService: GeneralService,
    private authenticationService: AuthenticationService,
    private logger: Logger,) {
   }
  
   get = {
    event: (paramsId: any): Promise<Table> =>{
      return new Promise((resolve, reject) => {
        this.tableParameters = new Table();
        this.tableParameters.tableID = paramsId.id
        this.tableParameters.gridId = '#agGridTable' + paramsId.id;
        this.generalService.getTableCharts(paramsId.id).subscribe(
            (response:ChartsResponse) =>{
                this.tableParameters.data = this.properData(response.data, response.config.rows_headers.indicators, response.config)
                this.tableParameters.headers = this.properHeader(response.config);
                this.tableParameters.config = response.config
                if (this.authenticationService.CurrentUserValue) {
                    this.tableParameters.is_logged = true;
                }
                this.generalService.getTableDetails(paramsId.id).subscribe((response: TableDetailsResponse) => {
                    this.tableParameters.pageTitle = response.data.nepali_name;
                    this.tableParameters.icon = response.data.icon_path;
                    resolve(this.tableParameters);
                })
                
            }
        )
        });

    }
  }

  getChartsConfig = {
      event:(tableId): Promise<BarChartStoredConfig[]> =>{
          return new Promise((resolve, reject)=>{
              this.generalService.getChartsConfig(tableId).subscribe(
                  (response) =>{
                      resolve(response.data)
                  }
              )
          } );
      }
  }

  properData(data,indicators, config:DataHeaderResponse){
    var newData = [];
    for (let index = 0; index < indicators.length; index++) {
        
        var indicator = indicators[index];
        var records = data.filter( record  => record.row === indicator.id )
        for (let index2 = 0; index2 < records.length; index2++) {
            var record = records[index2];
            if(this.grouplength < indicator.group.length){
                this.grouplength = indicator.group.length;
            }
            for (let group_index = 0; group_index < indicator.group.length; group_index++) {
                record[String(group_index)] = indicator.group[group_index]
                record['title'] = indicator.title;
            }
            try {
                record['order'] = indicator.header.ordering;    
            } catch (error) {
                record['order'] = 1
            }
            newData.push(record);
        }
        
    }
    newData.forEach(function (element) {
        config.col_headers.forEach(
            function (col_header){
                if (col_header.id in element){
                    element[col_header.id] = parseToFormat(element[col_header.id], col_header)
                }
            }
        )

        config.index_cols.forEach(
            function(index_col){
                element[index_col.col_id] = parseSelect(element[index_col.col_id], index_col)
            }
        )
    });
    return newData;
}


properHeader(config:DataHeaderResponse){
    let headers = [];
    headers.push({
        field: 'title',
        value: config.rows_headers.title
    });
    config.col_headers.forEach(function (col){
        headers.push({
            field: col.id,
            value: col.title
        })
    });
    config.index_cols.forEach(function (col){
        headers.push({
            field: col.col_id,
            value: col.title
        })
    });
    return headers;
}
}

function parseSelect(value, index_col:IndexColHeader){
    try{
        return index_col.options.find(function (opt){return opt.id == value }).nepali_name
    }
    catch{
        return value; // skip for extra values
    }
    
}

function parseToFormat(value, col_defn:ColHeader){
    if (col_defn.type == 'Number' || col_defn.type == "Float" || col_defn.type == "Rupees"){
        return parseFloat(value.replace(',',""))
    } else if (col_defn.type == "Select"){
        return col_defn.options.find(function (opt){return opt.id == value }).nepali_name
    }
}

