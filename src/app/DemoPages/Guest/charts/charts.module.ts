import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableChartsComponent } from './table-charts/table-charts.component';
import { GuestChartsRoutingModule } from './charts-router';
import { TableChartsService } from './table-charts.service';
import { GeneralService } from 'src/app/services/general.service';
import { Logger } from 'src/app/services/logger.service';
import { BarChartsComponent } from './bar-charts/bar-charts.component';
import { LineChartsComponent } from './line-charts/line-charts.component';
import { PieChartsComponent } from './pie-charts/pie-charts.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ChartsModule as Ng2ChartsModule } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [TableChartsComponent, BarChartsComponent, LineChartsComponent, PieChartsComponent],
  imports: [
    CommonModule, GuestChartsRoutingModule,
    NgApexchartsModule,
    Ng2ChartsModule,
    NgbModule,
  ],
  providers: [
    TableChartsService,
    GeneralService,
    Logger,
  ]
})
export class ChartsModule { }
