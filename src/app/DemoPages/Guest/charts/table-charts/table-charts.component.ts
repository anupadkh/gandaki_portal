import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {GeneralService} from "../../../../services/general.service";
import {TableChartsService} from '../table-charts.service';
import { Table } from '../../tables/dev-extreme/data/app.service';
import { Logger } from 'src/app/services/logger.service';



@Component({
  selector: 'app-table-charts',
  templateUrl: './table-charts.component.html',
  styleUrls: ['./table-charts.component.sass']
})
export class TableChartsComponent implements OnInit {

  table:  Table;
  columnNames: any[] = [];
  id; apiData; row_headers; index_cols; col_headers = []; filter_data = []; aggregate_function='sum'; subOptions = []; selectedIndexCol; selectedOption; pageTitle; chartType = 'line';renderingChart;
  public barChartLabels = [];

  constructor(
    private chartService: TableChartsService,
    private activatedRoute: ActivatedRoute,
    private generalService: GeneralService,
    private logger: Logger
  ) {
    this.table = new Table();
    this.activatedRoute.params.subscribe(paramsId => {
      
      this.id = paramsId.id;
      logger.log(this.id);

      this.chartService.get.event(paramsId).then( (table)=>{
        this.pageTitle = table.pageTitle;
        this.logger.log(table);
        this.renderCharts();
        }
      );

    });
 

   }

  ngOnInit() {
     }

  processDataForCharts(){
    var result = this.table.data.filter((x) => {
      let match_status = false;
      if(this.filter_data.length == 0){
        return x;
      }

      this.filter_data.forEach(datum => {
        if(match_status == true){
          return;
        }

        if(x[datum.col_id] == parseInt(datum.value)){
          match_status = true;
        }
      })

      // @ts-ignore
      if(match_status == true){
        return x;
      }
    })

    return result;
  }

  renderCharts(){
    this.chartService.getChartsConfig.event(this.id).then((charts)=>{

    })
  }

}
