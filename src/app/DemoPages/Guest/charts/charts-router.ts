import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableChartsComponent } from './table-charts/table-charts.component';

const routes: Routes = [
  {
    path: '',
    children: [
        {
            path: 'table/:id',
            component: TableChartsComponent,
            data:{
                title: 'Table Charts'
            }
          },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuestChartsRoutingModule { }