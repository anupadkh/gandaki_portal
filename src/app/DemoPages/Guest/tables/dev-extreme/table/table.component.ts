import {
  Component,
  OnInit,
  AfterViewInit,
  ChangeDetectionStrategy, ViewChild
} from '@angular/core';
import {
  ActivatedRoute
} from '@angular/router';
import {
  Service,
  Country,
  Table, TableHeader
} from '../data/app.service';
import {
  Logger
} from "../../../../../services/logger.service";
import { DxDataGridComponent } from 'devextreme-angular';
import { Observable } from 'rxjs';



@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'dev-app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [Service]
})


export class TableComponent implements OnInit, AfterViewInit {
  @ViewChild("stateGrid", {static:false}) invoiceDetailGrid: DxDataGridComponent;
  public table: Table;
  public render_table : Observable<Table>;
  public gen_columns: Array<any>;
  private gridId: any;
  constructor(
    private logger: Logger, 
    private server: Service,
    private activatedRoute: ActivatedRoute) {
      this.activatedRoute.params.subscribe(paramsId => {
        this.gridId = paramsId;
        this.render_table = new Observable<Table>(observer=>{
          this.table = this.server.getTable(this.gridId);
          observer.next(this.table)
        }
      );
      // this.table = this.render_table.
    })
  }
  

  ngAfterViewInit(): void {
    this.logger.log(this.table);
  }

  ngOnInit() {
      // this.server.get.event(this.gridId)
      //   .then(this.workResult).catch(
      //   this.debugOperation
      // )
      
    
    // this.logger.log(this.server.getTable());


  }

  // workResult(result:Table){
  //   // debugger;
  //   this.table = result;
  //   debugger;
  // }

  // debugOperation(error){
  //   debugger;
  // }
  
  // fetchEvent(): Promise<Table>{
  //   return ;
  // }

  manipulateParentColumn(columns, element) {
    // for (let index = 0; index < columns.length; index++) {
    //   if (columns[index].caption == )
    // }
    // columns.forEach(element2 => {
    //   if (element2.caption == element){
    //     element2
    //   }
    // });
  }

  customizeColumns() {
    // this.table.config.col_headers.forEach(element => {
    //   try {
    //     if (element.children.length != 0) {
    //       this.manipulateParentColumn(columns, element)
    //     }
    //   } catch (error) {

    //   }
    // });

    // for (let index = 0; index < columns.length; index++) {
    //   const element = columns[index];


    // }
    this.table.config.col_headers.forEach(col_header => {
      this.createColumnConfig(col_header);
    });
    debugger;
    // columns.push({
    //   caption:"Hello",
    //   isBand: true
    // })
    // for (let index = 0; index < columns.length-1; index++) {
    //   columns[index].ownerBand = columns.length -1;
    // }
    // // columns = this.gen_columns;
    // this.logger.log(columns);
  }

  createColumnConfig(header:TableHeader, index=0){
      if (header.children){
        const par_column = {
          caption : header.headerName,
          isBand: true,
          ownerBand : undefined
        };
        if (index!=0){
          par_column.ownerBand = index;
        }
        this.gen_columns.push(par_column);
        header.children.forEach(child => {
          this.createColumnConfig(child, this.gen_columns.indexOf(par_column));
        });
      }  else{
        const child_column = { 
          caption: header.headerName,
          ownerBand: undefined,
          dataSource: undefined,
          displayExpr: undefined,
          dataField: header.id
        }
        if (index !== 0){
          child_column.ownerBand = index
        }
        if (header.options.length > 0){
          child_column.dataSource = header.options;
          child_column.displayExpr = "nepali_name";
        }
        this.gen_columns.push(child_column);
      }
  }

  Check(node) {
    // debugger
    // this.logger.log(node);

    try {
      this.logger.log(node.children.length);
      this.logger.log(node);
      return true;
    } catch (error) {
      this.logger.log("Individual Header " + node.headerName)
      return false;

    }
  }


}