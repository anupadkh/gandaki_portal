import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DataComponent} from './data/data.component'
import { TableComponent } from './table/table.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Better Life',
      status: false
    },
    children: [
      {
        path: 'table',
        component: DataComponent
      },
      {
        path: 'details/:id',
        component: TableComponent,
        data: {
          title: 'Tables'
        },
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevRoutingModule {
}
