import { Injectable } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {FormService} from "../../../../../services/form.service";
import {GeneralService} from "../../../../../services/general.service";
import {AuthenticationService} from "../../../../../services/authentication.service";
import {Logger} from "../../../../../services/logger.service";

import {
    CollectionResponse,
    DataHeaderResponse,
    DataResponse,
    FormResponse,
    singleTableStateResponse,
    TableDetailsResponse
  } from "../../../../../models/user";
import { stringify } from '@angular/compiler/src/util';

export class DxHeader{
    caption: string;
    ownerBand?: any;
    isBand?: boolean;
    dataSource?: any;
    displayExpr?: any;
    width?: number;
    dataField?: string
}

export class Country {
    ID: number;
    Country: string;
    Area: number;
    Population_Urban: number;
    Population_Rural: number;
    Population_Total: number;
    GDP_Agriculture: number;
    GDP_Industry: number;
    GDP_Services: number;
    GDP_Total: number;
}

let countries: Country[] = [{
    "ID": 1,
    "Country": "Brazil",
    "Area": 8515767,
    "Population_Urban": 0.85,
    "Population_Rural": 0.15,
    "Population_Total": 205809000,
    "GDP_Agriculture": 0.054,
    "GDP_Industry": 0.274,
    "GDP_Services": 0.672,
    "GDP_Total": 2353025
}];

export class Options{
    id: number;
    name: string;
    nepali_name: string;
    ordering: number;
}

export class TableHeader {
    id: string;
    children?: TableHeader[];
    options: Options[];
    headerName: string;
    type: string;
}

export class Table{
    tableID: number;
    gridId?: string;
    pageTitle ?: string;
    icon: string;
    colState: {};
    is_logged: boolean;
    data: object[];
    collections: CollectionResponse;
    headers: any;
    config: DataHeaderResponse = new DataHeaderResponse();
}

@Injectable()
export class Service {

    private grouplength = 0;

    getCountries(): Country[] {
        return countries;
    }
    private tableParameters: Table;

    constructor(private activatedRoute: ActivatedRoute,
        private formService: FormService,
        private router: Router,
        private generalService: GeneralService,
        private authenticationService: AuthenticationService,
        private logger: Logger,
        ){
    }

    get = {
        event: (paramsId: any): Promise<Table> =>{
            return new Promise((resolve, reject) => {
            this.tableParameters = new Table();        
            this.tableParameters.tableID = paramsId.id
            this.tableParameters.gridId = '#agGridTable' + paramsId.id;

            this.generalService.getTableDetails(paramsId.id).subscribe((response: TableDetailsResponse) => {
                this.tableParameters.pageTitle = response.data.nepali_name;
                this.tableParameters.icon = response.data.icon_path;
                this.generalService.getSingleTableState(paramsId.id).subscribe((response: singleTableStateResponse) => {
                    this.tableParameters.colState = response.data.colState;
                })
            })

            if (this.authenticationService.CurrentUserValue) {
                this.tableParameters.is_logged = true;
            }

            this.formService.getPromiseData(paramsId.id).then(
                (response: DataResponse) =>{
                    this.tableParameters.data = this.properData(response.data, response.config.rows_headers.indicators, response.config)
                    this.tableParameters.headers = this.properHeader(response.config);
                    this.tableParameters.config = response.config
                    resolve(this.tableParameters);
                    debugger;
                }
            ).catch((error)=>{
                this.logger.log(error);
                reject(error);
            })
            // .subscribe((response: DataResponse) => {
            //     this.tableParameters.data = this.properData(response.data, response.config.rows_headers.indicators, response.config)
            //     this.tableParameters.headers = this.properHeader(response.config);
            //     this.tableParameters.config = response.config
            //     resolve(this.tableParameters);
            // })
            
            });
        }
    }

    
    getTable(paramsId){
        this.tableParameters = new Table();        
        this.tableParameters.tableID = paramsId.id
        this.tableParameters.gridId = '#agGridTable' + paramsId.id;

        this.generalService.getTableDetails(paramsId.id).subscribe((response: TableDetailsResponse) => {
            this.tableParameters.pageTitle = response.data.nepali_name;
            this.tableParameters.icon = response.data.icon_path;
            this.generalService.getSingleTableState(paramsId.id).subscribe((response: singleTableStateResponse) => {
                this.tableParameters.colState = response.data.colState;
            })
        })

        if (this.authenticationService.CurrentUserValue) {
            this.tableParameters.is_logged = true;
        }

        this.formService.getData(paramsId.id).subscribe((response: DataResponse) => {
            this.tableParameters.data = this.properData(response.data, response.config.rows_headers.indicators, response.config)
            this.tableParameters.headers = this.properHeader(response.config);
            this.tableParameters.config = response.config
        })

        return this.tableParameters;
    }

    properData(data,indicators, config){
        var newData = [];
        for (let index = 0; index < indicators.length; index++) {
            
            var indicator = indicators[index];
            // debugger
            var records = data.filter( record  => record.row === indicator.id )
            for (let index2 = 0; index2 < records.length; index2++) {
                var record = records[index2];
                if(this.grouplength < indicator.group.length){
                    this.grouplength = indicator.group.length;
                }
                for (let group_index = 0; group_index < indicator.group.length; group_index++) {
                    record[stringify(group_index)] = indicator.group[group_index]
                    record['title'] = indicator.title;
                }
                try {
                    record['order'] = indicator.header.ordering;    
                } catch (error) {
                    record['order'] = 1
                }
                

                this.logger.log(record);
                newData.push(record);
            }
            
        }
        return newData;
    }
    properHeader(config){
        return config;
    }
    
}
