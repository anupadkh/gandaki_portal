import { Component, OnInit } from '@angular/core';
import { DxDataGridModule } from 'devextreme-angular';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';


import { Service, Country } from './app.service';


@Component({
  selector: 'dev-app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css'],
  providers: [Service]
})
export class DataComponent implements OnInit {

  countries: Country[];

  constructor(service: Service) {
      this.countries = service.getCountries();
  }
  // constructor() { }

  ngOnInit() {
  }

}
