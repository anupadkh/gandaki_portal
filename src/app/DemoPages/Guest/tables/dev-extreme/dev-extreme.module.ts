import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataComponent } from './data/data.component';
import { DxDataGridModule, DxTemplateModule } from 'devextreme-angular';
import {DevRoutingModule} from './Dev-routing.module';

import {FormService} from "../../../../services/form.service";
import {GeneralService} from "../../../../services/general.service";
import {AuthenticationService} from "../../../../services/authentication.service";
import {Logger} from "../../../../services/logger.service";
import { TableComponent } from './table/table.component';
import { HeaderComponent } from './table/header/header.component';
import { Service  } from './data/app.service'



@NgModule({
  declarations: [DataComponent, 
    TableComponent, 
    HeaderComponent],
  imports: [
    CommonModule,
    DxDataGridModule,
    DevRoutingModule,
    DxTemplateModule,
  ],
  providers: [
    FormService,
    GeneralService,
    AuthenticationService,
    Logger,
    Service
  ]

})
export class DevExtremeModule { }
