import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {FormService} from "../../../../services/form.service";
import {GeneralService} from "../../../../services/general.service";
import {
  CollectionResponse,
  DataHeaderResponse,
  DataResponse,
  FormResponse, ListResponse, SingleObjectResponse,
  singleTableStateResponse,
  TableDetailsResponse
} from "../../../../models/user";
import Tabulator from 'tabulator-tables';

@Component({
  selector: 'app-menu-items',
  templateUrl: './menu-items.component.html',
  styleUrls: ['./menu-items.component.sass']
})
export class MenuItemsComponent implements OnInit {

  images = [];
  menuItems = [];
  public responseData ;
  forms_count = [];
  tables_count = [];
  public forms_length = 0;
  public tables_length = 0;
  id;
  public has_child = true;

  constructor( private generalService: GeneralService, private router: Router, private activatedRoute: ActivatedRoute, private formService: FormService) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(paramsId => {
      this.id = paramsId.id;
      this.responseData = {"tables":[], "sub_forms":[]};

      this.formService.getFormDetails(this.id).subscribe((response: SingleObjectResponse) => {
        this.responseData = response.data;
        try {
          if (response.data['tables'].length == 0 && response.data['sub_forms'].length == 0)  {
            this.has_child = false;
          }
        } catch (error) {
          
        }
        
      })
    });
  }

  navigateToTableList(tableId){
    this.router.navigate(['/guest/tables/'+tableId]);
  }

  navigateToTable(tableId){
    this.router.navigate(['/guest/tables/details/'+tableId]);
  }

}
