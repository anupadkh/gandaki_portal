import {AfterViewInit, Component, HostBinding, HostListener, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder} from "@angular/forms";
import {FormService} from "../../../services/form.service";
import {GeneralService} from "../../../services/general.service";
import {
  CollectionResponse,
  DataHeaderResponse,
  DataResponse,
  FormResponse,
  singleTableStateResponse,
  TableDetailsResponse,
  DeleteResponse,
  SingleModifiedTable,
  ModifiedTableResponse
} from "../../../models/user";
import Tabulator from 'tabulator-tables';
import {AuthenticationService} from "../../../services/authentication.service";
import {options} from "ionicons/icons";
import { Logger } from 'ag-grid-community';
import { FullscreenService } from '../../Admin/tables/full-screen.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styles: [`
    .tabulator-group-level-0{
      background: red !important;
      color: white;
    }
    .tabulator-group-level-1{
    background: #2873b9 !important;
    color: white;
    }
    #my-tabular-table .tabulator .tabulator-header .tabulator-col .tabulator-col-content .tabulator-col-title {
      white-space: normal;
      overflow-wrap: break-word;
      text-overflow: clip;
    }
    #my-tabular-table.tabulator-group-level-0{
      color: white;
      background-color: #273282;
    }
    .tabulator-row.tabulator-group{
      color: white;
      background-color: #273282;
    }
    .tabulator-row .tabulator-group .tabulator-group-level-0 .tabulator-group-visible .tabulator-row-odd{
      color: white;
      background-color: green;
    }
    `]
})
export class TablesComponent implements OnInit {
  gridId;
  id;
  private gridApi;
  private gridColumnApi;
  defaultColDef;
  sideBar;
  statusBar;

  ColumnAddForm;
  loading;
  submitted = false;
  returnUrl: string;
  error = '';
  columnDefs = [];
  colData = [];

  rowDefs;
  rowData = [];
  fields = [];

  title = 'app';
  autoGroupColumnDef;
  editType;
  private editingRowIndex;
  updatedData;
  parentObject;
  subFormObjects;
  apiData;
  row_headers;
  col_headers;
  index_cols;
  colIds;
  empty_table;
  indexIds;
  pageTitle;
  buttonText = 'Save Data';
  btnClass;
  colState;
  groupState;
  sortState;
  filterState;
  groups = [];
  tabulatorTable : any;
  collections;
  is_logged = false;

  icon; tableID; activeFilter=false; collect_title; collect_icon_path; collect_sub_title;
  public activated = false;
  public navOptions = []; public tableOptionId = false; baseTableId; modifiedTableId; public modifiedTables:SingleModifiedTable[];

  @Input() tableData: any[] = [
  ];
  @Input() columnNames: any[] = [ //Define Table Columns
  ];

  @Input() height: string = '311px';

  tab = document.createElement('div');

  constructor(
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private formService: FormService,
    private router: Router,
    private generalService: GeneralService,
    private authenticationService: AuthenticationService,
    private fullScreenService: FullscreenService
  ) {
    this.autoGroupColumnDef = { minWidth: 200 };
    this.colIds = [];
    this.indexIds = [];
    this.collections = [];
    this.loading = true;
    this.apiData = []
  }

  ngOnInit(): void {
    this.collections = [];
    this.activatedRoute.params.subscribe(paramsId => {
      
      this.baseTableId = paramsId.id.split('_')[0];
      
      try {
        this.modifiedTableId = paramsId.id.split('_')[1];
        try{
          this.tableOptionId = paramsId.options;
        } catch (error){

        }
      } catch (error) {
        
      }
      
      this.id = paramsId.id;
      this.gridId = '#agGridTable'+this.id;
      this.generalService.getSingleTableState(this.id).subscribe((response: singleTableStateResponse) => {
        try{this.colState = response.data.colState;} catch{};
        this.generalService.getTableDetails(this.baseTableId).subscribe((response: TableDetailsResponse) => {
          this.pageTitle = response.data.nepali_name;
          this.icon = response.data.icon_path;
          this.tableID = response.data.id;
          this.renderTable(true);
        })
      })
      

      

      if (this.authenticationService.CurrentUserValue) {
        this.is_logged = true;
      }
      
      this.formService.getModifiedTableFromMain(String(this.id).split('_')[0], ).subscribe((response: ModifiedTableResponse)=>{
        this.modifiedTables = response.data;
        if (String(this.id).indexOf('_') != -1){
          this.modifiedTables.forEach(elem=>{
            if(elem.id == (this.id).split('_')[1]){
              this.navOptions = elem.index_col_members;
            }
          })
          // debugger;
        }
      })
    });

  }

  private drawTable(): void {
    this.columnNames.forEach(columnName => {
      this.handleVisibility(columnName);
    })

    if(this.groups.length > 1){
      this.formService.getCollectionList(this.tableID).subscribe((response: CollectionResponse) => {
        this.collections = response.data;
        this.tabulatorTable.options.groupBy=this.groups;
        // console.log(this.tabulatorTable.options.groupBy);
        // console.log(this.collections, this.id);
      })
    } else{
      this.collections = [];
    }
    var visible_columns = this.columnNames.filter((col)=>{
      return col.visible !== false;
    })
    this.columnNames = visible_columns;
    // debugger;
    if (visible_columns.length >= 11){
      // var responsive = this.columnNames.slice(10, this.columnNames.length)
      // responsive.forEach((col)=>{
      //   col.responsive = 
      // })
      // this.columnNames.splice(0,0,
      //   {formatter:"responsiveCollapse", width:30, minWidth:30, hozAlign:"center", resizable:false, headerSort:false}
      // )
      
    }else{
    }
    
    this.tabulatorTable = new Tabulator("#my-tabular-table", {
      data:this.apiData,           //load row data from array
      layout:"fitColumns",      //fit columns to width of table
      tooltips:true,            //show tool tips on cells
      addRowPos:"top",          //when adding a new row, add it to the top of the table
      history:true,             //allow undo and redo actions on the table
      movableColumns:true,      //allow column order to be changed
      resizableRows:true,
      groupBy:this.groups,
      columnHeaderVertAlign: "bottom", //allow row order to be changed
      // initialSort:[             //set the initial sort order of the data
      //   {column:"name", dir:"asc"},
      // ],
      // responsiveLayout:"collapse",
      columns:this.columnNames,
      invalidOptionWarnings: false,
      initialSort:[
        {column:"order", dir:"desc"},
        {column:"order2", dir:"asc"}
      ],
      tableBuilt:function(){

        this.redraw(true);
      },
    });
  }


  renderTable(isOnInit) {
    this.columnNames = [];
    this.apiData = [];
    // debugger
    this.formService.getDataHeaders(this.id, this.tableOptionId).subscribe((response: DataHeaderResponse) => {
      // console.log(response);
      this.row_headers = response.rows_headers;
      this.col_headers = response.col_headers;
      this.index_cols = response.index_cols;
      this.empty_table = response.empty_table;
      this.rowData = []

      // this.gridApi.setRowData([]);

      if (isOnInit == true) {
        this.colData = [];
        this.groups = [];
        // this.gridApi.setColumnDefs(this.colData);

        this.colData.push({
          title: '_id', field: '_id', visible: false
        });
        this.colData.push({
          title: "order", field: "order", visible: false
        });
        this.colData.push({
          title: "order2", field: "order2", visible: false
        });
        this.colIds.push('_id');
        
        if(this.row_headers.indicators[0]){
          for(let i = 0; i< this.row_headers.indicators[0].group.length; i++){
            this.colData.push(
              {field: 'group'+i, title: 'group'+i, visible: false}
            )
            this.groups.push('group'+i);
            this.colIds.push('group'+i);
          }
        }

        // console.log(this.groups);

/*        this.colData.push(
          {headerName: 'group', field: 'group', title: 'group', pinned: 'left', visible: false}
        )
        this.colIds.push('group');*/

        this.colData.push(
          {field: 'row', title: this.row_headers.title}
        )
        this.colIds.push(this.row_headers.title);
        this.indexIds.push('row');

        this.index_cols.forEach(value => {

          if (value.options.length != 0) {
            const currentOptions = [];
            value.options.forEach(option => {
              currentOptions.push(option.nepali_name);
            })

            this.colData.push(
              {
                // headerName: value.title,
                field: value.col_id,
                // sortable: true,
                // filter: true,
                title: value.title,
                // cellEditor: 'agSelectCellEditor',
                cellEditorParams: {
                  values: currentOptions,
                },
              });
          } else {
            this.colData.push(
              {
                // headerName: value.title,
                field: value.col_id,
                // sortable: true,
                // filter: true,
                title: value.title
              });
          }
          this.colIds.push(value.col_id);
          this.indexIds.push(value.col_id);
        })

        this.col_headers.forEach(items => {
          this.modifyColumnHeaders(items);
          this.modifyColumnHeadersForTabulator(items);
          this.colData.push(items)
        })

        //Modify the columns as per the col state
        if(this.colState != null){
          this.colState.forEach(columnState => {
            this.colData.forEach(columnData => {
              if(columnData.columns){
                columnData.columns.forEach(column => {
                  if(column.id == columnState.colId){
                    column.width = (columnState.width);
                  }
                })
              }
              else if(columnData.field == columnState.colId){
                columnData.width = ( columnState.width);
              }
            })
          })
        }
        this.columnNames = this.colData;

      } else{
        this.redrawTable();
      }



      this.formService.getData(this.id, this.tableOptionId).subscribe((dataResponse: DataResponse) => {
        this.apiData = dataResponse.data;
        this.apiData.forEach(apiDatum => {
          this.row_headers.indicators.forEach(indicator => {
            if(indicator.id == apiDatum.row){
              apiDatum.row = indicator.title;
              for (let i =0; i<indicator.group.length;i++){
                apiDatum['group'+i] = indicator.group[i];
              }
              try {
                apiDatum['order'] = indicator.header.ordering;
                apiDatum['order2']  = indicator.header.id;
              } catch (error) {
                apiDatum['order'] = 0
                apiDatum['order2'] = 10000
              }
            }
          })
          this.index_cols.forEach(index_col => {
            if(index_col.options){
              index_col.options.forEach(option => {
                if(apiDatum[index_col.col_id] == option.id){
                  apiDatum[index_col.col_id] = option.nepali_name;
                }
              })
            }
          })
        })
        
        this.drawTable();
      });
    })
  }

  modifyColumnHeaders(obj){
    obj.editable = false;
    obj.title = obj.headerName;
    if(obj.hasOwnProperty('children')){
      let columnChildrens = obj.children;

      columnChildrens.forEach(columnChildren => {
        if(columnChildren.type == 'Select'){
          const currentOptions = [];
          try {
            columnChildren.options.forEach(option => {
              currentOptions.push(option.nepali_name);
            })  
          } catch (error) {
            console.log(columnChildren);
          }
          

          columnChildren.editable = false;
          // columnChildren.cellEditor =  'agSelectCellEditor';
          columnChildren.cellEditorParams = {
            values: currentOptions,
          };
        }
        this.modifyColumnHeaders(columnChildren);
      })
    }
    return null;
  }



  modifyColumnHeadersForTabulator(obj){
    if(obj.hasOwnProperty('children')){
      obj.columns = obj.children;
      obj.children.forEach(child => {
        this.modifyColumnHeadersForTabulator(child);
      })

    }
    obj.headerSort = false;
    
    // delete(obj.headerName);
    // delete(obj.type);
    // delete(obj.sortable);
    // delete(obj.filter);
    // delete(obj.options);
    // delete(obj.children);
    // delete(obj.id)

    return null;
  }

  handleVisibility(obj){
    if(obj.field == '_id'){
      obj.visible = false;
      obj.download = false
    }
    function checkValueEmpty(x) {
      if (x == "" || x == undefined){
        return true;
      }
    }

    obj.formatter = function (cell, formatterParams, onRendered) {
      if (checkValueEmpty(cell.getValue())){
        return "";
      }
      if(obj.type == 'Rupees'){
        obj.align = "right";
        return "<span style='font-family: Fontasy Himali;float:right'>"+ 'रू '+ parseFloat(cell.getValue()).toFixed(2) +"</span>";
      }
      else if(obj.type == 'Number'){
        return "<span style='font-family: Fontasy Himali;float:right'>"+ cell.getValue() +"</span>";
      }
      else if(obj.type == 'Float'){
        return "<span style='font-family: Fontasy Himali;float:right'>"+ parseFloat(cell.getValue()).toFixed(2) +"</span>";
      }
      else if(obj.type == 'Dollars'){
        obj.align = "right";
        return "<span style='float:right'>"+ '$ '+ cell.getValue() +"</span>";
      }
      else if(obj.type == 'Percentage'){
        let percentageValue =  cell.getValue();
        return "<span style='font-family: Fontasy Himali;float:right'>"+ parseFloat(percentageValue).toFixed(2) + '% ' +"</span>";
      }
      else if(obj.type == 'Select'){
        let value = cell.getValue();
        try {
          obj.options.forEach(option => {
            if(option.id == value){
              value = option.nepali_name;
            }
          });  
        } catch (error) {
          console.log(obj);
        }
        
        return value;
      }
      else{
        return cell.getValue();
      }
    }

    if(this.colState){
      this.colState.forEach(col => {
        if(col.colId == obj.field){
          if(col.hide == true){
            obj.visible = false;
          }
        }
      });
    }

    if(obj.hasOwnProperty('columns')){
      obj.columns.forEach(column =>{
        this.handleVisibility(column);
      })
    }

    return null;
  }

  exportTabulatorTable(){
    this.tabulatorTable.download("xlsx", this.pageTitle+".xlsx", {sheetName:"Table"});
  }

  addFilterToTabulator(text){
    this.tabulatorTable.clearFilter();
    this.tabulatorTable.addFilter('group0', '=', text);
  }

  resetFilter(){
    this.tabulatorTable.clearFilter();
  }

  goToDataEntry(option_id){
    if (option_id){
      this.router.navigate(['/admin/tables/details/'+this.id+'/' + option_id])
    }else{
      this.router.navigate(['/admin/tables/details/'+this.id])
    }
    
  }

  goToModifiedTable(col_id){
    this.router.navigate(['/guest/tables/details/'+this.id +'_' + col_id])
  }

  goToModifiedOptionTable(option_id){
    this.router.navigate(['/guest/tables/details/'+this.id +'/' + option_id])
  }

  navigateToCharts(){
    this.router.navigate(['admin/tables/charts/'+this.id])
  }



  redrawTable(){
    this.tabulatorTable.redraw(true);
  }

  deleteData(cellRow) {
    if ( confirm("Are you Sure to Delete "+ cellRow._row.data['row'] + " ? ")) {
      this.formService.deleteFormValues(this.id, 
        {
        "data": [cellRow._row.data["_id"]], "delete":true 
        }).subscribe(
          (dataResponse:DeleteResponse) => {
            if (dataResponse.data == 1){
              cellRow.delete();
            } else{
              alert("Unauthorized Access");
            }
          }
        )
      
    }
  }

  public activateDelete() {
    this.activated = !this.activated;  
    
    if (this.is_logged && this.activated){
      this.tabulatorTable.addColumn({title:"X", field:"delete",formatter:"buttonCross", width:40, align:"center", cellClick:(e, cell) =>{
        this.deleteData(cell.getRow());
      }})
    } else{
      this.tabulatorTable.deleteColumn("delete");
    }
    // debugger
    this.redrawTable();
    
  }
  
  isFullscreen = false;

  isActive = false;

  openFullscreen(): void {
    this.fullScreenService.toggle();
    this.isFullscreen = !this.isFullscreen;
  }

  closeFullscreen(): void {
    this.fullScreenService.toggle();
    this.isFullscreen = !this.isFullscreen;
  }
  
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.redrawTable();
  }

}
