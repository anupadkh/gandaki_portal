import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ConfigActions } from '../../../ThemeOptions/store/config.actions';
import { ThemeOptions } from '../../../theme-options';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent implements OnInit, AfterViewInit {

  constructor(public globals: ThemeOptions,
    public configActions: ConfigActions) {
  }

  ngOnInit() {
    
  }

  ngAfterViewInit(){
    // this.globals.toggleFixedFooter = false;
  }

}
