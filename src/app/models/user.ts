export class User {
  user: {
    id: number,
    username: string,
    email: string
  };
  token?: string;
}

export class DeleteResponse{
  data: number;
}

export class ExcelTableResponse {
  fields: [];
  data: [];
  columns: [];
}

export class ListResponse {
  method: {};
  fields: [];
  columns: [];
  data: [];
}

export class Options{
  id: number;
  name: string;
  nepali_name: string;
  ordering: number;
}

export class ColHeader {
  id: string;
  children?: ColHeader[];
  options: Options[];
  headerName: string;
  type: string;
  chartsData ?: any;
  header?: any;
  title ?: string;
}

export class IndexColHeader{
  col_id: string;
  options: Options[];
  title: string;
}

export class DataHeaderResponse {
  rows_headers:{
    title: string,
    indicators: Indicator[]
  };
  col_headers: ColHeader[];
  index_cols: IndexColHeader[];
  empty_table?: [];
}


export class SingleObjectResponse {
  method: {};
  fields: [];
  columns: [];
  data: {};
}

export class TableDetailsResponse {
  method: {};
  fields: [];
  columns: [];
  data: {
    name: string,
    nepali_name: string,
    id: string,
    icon_path: string
  };
}

export class FormResponse {
  formats: string;
  data: {
    id: string
  };
}

class Indicator{
  group: [];
  id: string;
  title:string;
}


export class DataResponse {
  data: [];
  config: {
    rows_headers: {
      title: string,
      indicators: Indicator[]
    },
    index_cols: IndexColHeader[],
    col_headers: ColHeader[],
    empty_table ?: []
  }
}

export class ChartsResponse {
  data: [];
  config: {
    rows_headers: {
      title: string,
      indicators: []
    },
    index_cols:[],
    col_headers: []
  }
}

export class TableStateResponse {
  data: {
    _id: string
  }
}

export class singleTableStateResponse {
  data:{
    _id: String,
    tableId: String,
    colState: [
      {
        colId: String,
        hide: boolean,
        aggFunc: any,
        width: Number,
        pivotIndex: any,
        pinned: any,
        rowGroupIndex: any,
        flex: number
      }
    ],
    groupState: [
      {
        groupId: String,
        open: boolean
      }
    ],
    sortState: [],
    filterState: [],
  }
}

export class SinglePageResponse {
  data: {
    _id: string,
    slug: string,
    content: string
  }
}

export class searchResponse {
}


export class CollectionResponse {
  data:[
    {
      id: number,
      icon_name: string,
      icon_class: string,
      icon_path: string,
      icon_description: string,
      name: string,
      nepali_name: string,
      ordering: number,
      higher_collection: any,
      related_table: any
    }
  ]
}

export class SingleModifiedTable{
  id: number;
  published: boolean;
  table: number;
  index_col: number;
  removed_cols: number[];
  new_cols: number[];
  index_col_obj: Options;
  index_col_members: Options[]
}

export class ModifiedTableResponse{
  data: SingleModifiedTable[

  ]
}