import { Component, OnInit } from '@angular/core';
import { DragDropModule, CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";

@Component({
  selector: 'app-test',
  // templateUrl: './test.component.html',
  // styleUrls: ['./test.component.sass'],
  styles : [`
    .item{
      height:150px;
      width: 100% !important;
      border: 1px solid green;
      display: inline-flex;
      justify-content: center;
      align-items: center:
    }
    .itemlist{
      width: 50%;
      border: 1px solid blue;
    }
  `],
  template: `
  
  <div class="card main-card mb-12">
    <div style="display:flex">
      <div cdkDropList 
      [cdkDropListData] = "numbers"
      [cdkDropListConnectedTo] = "otherList"
       (cdkDropListDropped)="drop($event);" 
       class="itemlist"
       #firstList="cdkDropList"
       >
        <div cdkDrag *ngFor="let number of numbers" class="item">{{number}}</div>
      </div>
    
    
      <div 
      [cdkDropListData] = "othernumbers"
      [cdkDropListConnectedTo] = "firstList"
      #otherList="cdkDropList"
      cdkDropList (cdkDropListDropped)="drop($event);" 
      class="itemlist" 
      style="border:red solid 3px;">
        <div cdkDrag *ngFor="let number of othernumbers" class="item" style="color:red">{{number}}</div>
      </div>
    </div>
  </div>
  `
})
export class TestComponent implements OnInit {

  title = "NeW Drag And Drop";
  numbers : number[] = [];
  othernumbers : number[] = [];
  constructor() {
      for (let i=0; i<10; i++){
        this.numbers.push(i);
      }
   }



  ngOnInit() {
  }

  drop (event: CdkDragDrop<number []>){
    if (event.previousContainer !== event.container){
      transferArrayItem(event.previousContainer.data, event.container.data,  event.previousIndex, event.currentIndex);
      console.log(this.numbers); console.log(this.othernumbers);
    }else{
      moveItemInArray(this.numbers, event.previousIndex, event.currentIndex);
    }
    
  }
}
