import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TestRoutingModule} from './test-routing.module';
import {TestComponent} from './test.component';
import {DragDropModule} from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [
    TestComponent,
  ],
  imports: [
    CommonModule,
    TestRoutingModule,
    DragDropModule,
  ]
})
export class TestModule { }
