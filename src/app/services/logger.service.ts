import { Injectable } from '@angular/core';
import {AuthenticationService} from './authentication.service';


@Injectable()
export class Logger{
    constructor(private authenticationService: AuthenticationService){
    }
    debug_mode = true

    log(a){
        if (this.debug_mode){
            console.log(a);
        }
    }
}
