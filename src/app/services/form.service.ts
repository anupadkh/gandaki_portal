import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {delayedRetry} from './retry';

@Injectable({
    providedIn: 'root'
})
export class FormService {
    constructor(private httpClient: HttpClient) {
    }

    handleError(error: HttpErrorResponse) {
        let errorMessage = 'Unknown error!';
        if (error.error instanceof ErrorEvent) {
            // Client-side errors
            errorMessage = `Error: ${error.error.message}`;
        } else {
            // Server-side errors
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
    }

    getFormValues() {
        return this.httpClient.get(`${environment.apiUrl}/portal/form?name=forms&list&design=1`)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    deleteFormValues(id,data){
        return this.httpClient.post(`${environment.apiUrl}/portal/data?id=`+ id.toLocaleString(), 
            data
        ).pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    saveFormValues(data) {
        return this.httpClient.post(`${environment.apiUrl}/portal/form?name=forms`, data)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    getFormDetails(id) {
        return this.httpClient.get(`${environment.apiUrl}/portal/form?name=forms&id=` + id + `&design=1`)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    saveTableValues(data) {
        return this.httpClient.post(`${environment.apiUrl}/portal/form?name=tableGroup`, data)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    getTableGroupValues() {
        return this.httpClient.get(`${environment.apiUrl}/portal/form?name=tableGroup&list&design=1`)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    getQuestionColumns(id) {
        return this.httpClient.get(`${environment.apiUrl}/portal/form?name=tableDetail&design=1&id=` + id)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    saveQuestionColumns(data) {
        return this.httpClient.post(`${environment.apiUrl}/portal/form?name=questionsColumns`, data)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    getDataHeaders(id, options_id = false) {
        id = this.transformid(id,options_id);
        return this.httpClient.get(`${environment.apiUrl}/portal/parserApi?id=` + id)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    transformid(id, options_id){
        if (options_id !== false) {
            id = String(id) + '&option_id=' + options_id;
            return id;
        } else{
            return String(id);
        }
        
    }

    getData(id, options_id = false) {
        id = this.transformid(id, options_id)
        return this.httpClient.get(`${environment.apiUrl}/portal/data?custom=1&id=` + id)
            .pipe(delayedRetry(1000, 3), catchError(this.handleError));
    }

    getPromiseData(id,options_id=false){
        id = this.transformid(id, options_id)
        let url = `${environment.apiUrl}/portal/data?custom=1&id=` + id
        return this.httpClient.get(url).toPromise()
    }

    saveData(id, data, extra=[]){
        return this.httpClient.post(`${environment.apiUrl}/portal/data?id=` + id +'&extra_params=[' + extra.toLocaleString()+']', {'data':data})
            .pipe(retry(0), catchError(this.handleError));
    }

  getCollectionList(id) {
    return this.httpClient.get(`${environment.apiUrl}/portal/form?name=collectionInTable&id=` + id)
      .pipe(delayedRetry(1000, 3), catchError(this.handleError));
  }

  getIndicators(id) {
    return this.httpClient.get(`${environment.apiUrl}/portal/form?name=indicators&design=1&id=` + id)
      .pipe(delayedRetry(1000, 3), catchError(this.handleError));
  }

  saveIndicators(data) {
    return this.httpClient.post(`${environment.apiUrl}/portal/form?name=indicators`, data)
      .pipe(delayedRetry(1000, 3), catchError(this.handleError));
  }

  getModifiedTableFromMain(id) {
    return this.httpClient.get(`${environment.apiUrl}/portal/form?name=tabcustom&id=`+ id)
      .pipe(delayedRetry(1000, 3), catchError(this.handleError));
  }

  getModifiedTable(id){
    return this.httpClient.get(`${environment.apiUrl}/portal/form?name=tabCustom&id=`+ id)
    .pipe(delayedRetry(1000, 3), catchError(this.handleError));
  }
}
